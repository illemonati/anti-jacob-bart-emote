const scaleThingDiv = async (scale) => {
    const thingDiv = document.getElementById('thing');
    const rect = thingDiv.getBoundingClientRect();
    if (!scale) {
        let largestRectSide = 0;
        let coorispondingScreenSide = 0;
        if (rect.width > rect.height) {
            largestRectSide = rect.width;
            coorispondingScreenSide = window.innerWidth;
        } else {
            largestRectSide = rect.height;
            coorispondingScreenSide = window.innerHeight;
        }
        scale = coorispondingScreenSide / largestRectSide;
        console.log(largestRectSide, coorispondingScreenSide, scale);
    }
    thingDiv.style.transform = `scale(${scale})`;
};

const sleep = (ms) => new Promise((r) => setTimeout(r, ms));

const fontChange = async () => {
    const fonts = ['Creepster', 'Butcherman'];
    let i = 0;
    while (true) {
        document.body.style.fontFamily = fonts[i];
        await scaleThingDiv(1);
        console.log(fonts[i]);
        i++;
        if (i > fonts.length - 1) {
            i = 0;
        }
        await scaleThingDiv();
        await sleep(3000);
    }
};

const noChange = async () => {
    const no = document.getElementById('no');
    while (true) {
        no.style.display = no.style.display === 'none' ? '' : 'none';
        await sleep(1000);
    }
};

export const main = async () => {
    fontChange().then();
    noChange().then();
};
main().then();
